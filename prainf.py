class prainf:
  class MemoryErr(Exception): pass
  class LoopErr(Exception): pass
  class UnknownCmdErr(Exception): pass
  def __init__(
    self,
    source: str,
    memory: int = 30000
  ):
    self.out = ''
    self.src = source
    self.ci = 0
    self.cs = [0] * memory
    self.cmds = '[>+-.,<]'
  def rmNonCmd(self, src):
    clean = ''
    for c in src:
      if c in self.cmds:
        clean += c
    return clean
  def plus(self):
    self.cs[self.ci] += 1
  def detract(self):
    self.cs[self.ci] -= 1
    if self.cs[self.ci] < 0:
      self.cs[self.ci] = 256 - 1
  def ncell(self):
    self.ci += 1
  def pcell(self):
    self.ci -= 1
    if self.ci < 0:
      raise self.MemoryError("Cell "+str(self.ci))
  def put(self):
    self.out += chr(self.cs[self.ci])
  def get(self):
    self.cs[self.ci] = ord(str(input()))
  def run(self):
    inloop = False
    loop = ''
    for c in self.rmNonCmd(self.src):
      if c == '[':
        inloop = True
        continue
      elif c == ']' and inloop:
        cnt = self.cs[self.ci]
        while cnt:
          for c in loop:
            if c == '>':
              self.ncell()
            elif c == '<':
              self.pcell()
            elif c == '+':
              self.plus()
            elif c == '-':
              self.detract()
            elif c == '.':
              self.put()
            elif c == ',':
              self.get()
          cnt = self.cs[self.ci]
        loop = ''
        inloop = False
      elif inloop:
        loop += c
        continue 
      if not inloop:
        if c == '>':
          self.ncell()
        elif c == '<':
          self.pcell()
        elif c == '+':
          self.plus()
        elif c == '-':
          self.detract()
        elif c == '.':
          self.put()
        elif c == ',':
          self.get()
        elif c == ']':
          raise self.LoopErr("Unknown ']' at "+str(self.src.index(']')+1)+". charatcher")
        else:
          raise self.UnknownCmdErr(str(c))
    return self.out
